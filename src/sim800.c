#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/select.h>
#include <termios.h> 
#include <string.h>
#include <assert.h>
#include <sys/mman.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "sim800.h"
#include "iniparser.h"


int fd;
int fdajax;

fd_set fs;
fd_set fsajax;

struct timeval timeout;
struct termios term;
struct termios termajax;

int voice_counter= 0;

char ttyAjax[64];
char ttyModem[64];

char bufajax[BUF_SIZE];
char buf[BUF_SIZE];

char modemAnswer[BUF_SIZE];
int idxAnswer;

char ajaxAnswer[BUF_SIZE];
int idxAjax;

int status = ST_MODEM_START;
int ajaxstatus = ST_AJAX_START;

char phone[PHONE_COUNT][32];
int actphone= 0;
int actcycle = 0;
int maxcycle = 5;

char ajaxSmokeId[MAXID];
char ajaxMotionId[MAXID];
char ajaxDoorId[MAXID];
char ajaxKeyId[MAXID];
int ajaxAlarm = AJAX_ALARM_NO;

char pin[16];
char music[6][48];

int test;

/*
int createMemory()
{
	int shm, len, cmd, mode = 0;
    char *addr;

        len = strlen("TEST TEST TEST");
        len = (len<=SHARED_MEMORY_OBJECT_SIZE)?len:SHARED_MEMORY_OBJECT_SIZE;
        mode = O_CREAT;
	cmd = SHM_CREATE;
	
	if ( (shm = shm_open(SHARED_MEMORY_OBJECT_NAME, mode|O_RDWR, 0777)) == -1 ) {
        	perror("shm_open");
	        return 0;
    	}

    	if ( cmd == SHM_CREATE ) {
        	if ( ftruncate(shm, SHARED_MEMORY_OBJECT_SIZE+1) == -1 ) {
            		perror("ftruncate");
		        return 1;
        	}
    	}

    addr = mmap(0, SHARED_MEMORY_OBJECT_SIZE+1, PROT_WRITE|PROT_READ, MAP_SHARED, shm, 0);
    if ( addr == (char*)-1 ) {
        perror("mmap");
        return 1;
    }

    memcpy(addr, "TEST TEST TEST", len);
    addr[len] = '\0';
    printf("Shared memory filled in. You may run '-- print' to see value.\n");
	return 1;

}
*/
/*
int getphonedata( char *filename)
{
	FILE *fp;
	char buf[256];
	
	memset( buf, 0, 256);
	if ((fp = fopen( filename, "r"))==NULL) {
		return -1;
	}
	int rbytes= fread( buf, sizeof(char), 256, fp);
	fclose(fp);	
	
	char * pch;

	pch = strtok (buf,"\n");
	int actphone= 0;
	while (pch != NULL)
	{
		strcpy( phone[actphone++], pch);
		pch = strtok (NULL, "\n");
	}		
	
	for ( int i= 0; i < actphone; i++)
	 printf("Phone: %s\n", phone[i];
	 
	return 0;
	
} 
* */
void parseNetstatus( char *txt)
{
	int  _status; 
	
	if ( strlen(txt) < strlen("+CREG: 0,5") )
	  return;

	_status = txt[9] - '0';
	modem.netstatus = _status;
}
void parseCpas ( char *txt)
{
	int  _status; 
	
	if ( strlen( txt) < strlen("+CPAS: 0"))
		return;
	_status = txt[7] - '0';
	modem.status = _status;
}

void parseLevel(char *txt)
{
	//+CSQ: 17,0
	int i = 0;
	int l = strlen( txt);
	char name[MAXNAME];
	int level= 0;
	int k = 1;
	int islevel= 0;
	
	for ( i=l-1; i >=0 ; i--)
	{
	//printf ("\n %c\t%d\t%d\n", txt[i], k, level);
		if ( txt[i] == ','  ) // "
		{
			if ( islevel == 0)
				islevel = 1;
			else
			  break;
		}
		else if ( txt[i] == ' ')
			break;
		else
			if ( islevel == 1)
			{
				level += (txt[i] -'0') *k;
				k *= 10;
			}
	}
	modem.level = level;
	//printf ("\n level = %d\n", level);
	
}

char *getname( char *txt)
{
	int i = 0;
	int isname = 0;
	int l = strlen( txt);
	char name[MAXNAME];
	int j= 0;
	memset( name, 0, MAXNAME);
	
	for ( i=0; i < l ; i++)
	{
		if ( txt[i] == '"'  ) // "
		{
			if ( isname == 0)
				isname = 1;
			else
			  break;
		}
		else
			if ( isname == 1)
				name[j++] = txt[i];
	}
	//printf("\n[%s]\n", name);
	return strdup(name);
}

void parseCops(char *txt)
{
	//+COPS: 0,0,"E-Plus"
	char * tmp = getname(txt);
	//printf("\n[%s]\n", tmp);
	strncpy( modem.netname, tmp, strlen(tmp));	
	free( tmp);
	//strcpy(modem.netname, "Kkk");
}
void parseSIM(char *txt)
{
	char * tmp = getname(txt);
	//printf("\n[%s]\n", tmp);
	strncpy( modem.simname, tmp, strlen(tmp));	
	free( tmp);
}

void hangup()
{
	sendData("ATH\r");
}

int callAlarm( char *tel)
{
	sendData( phone[actphone] );
	status = ST_MODEM_CALL;
}

int checkAlarmStatus()
{
	FILE *fp;
	char abuf[32];
	
	memset( abuf, 0, 32);
	if ((fp = fopen( FILE_ALARM, "r"))==NULL) {
		return ALARMERROR;
	}
	int rbytes= fread( buf, sizeof(char), 32, fp);
	fclose(fp);		
	
    if ( strncmp( trimwhitespace(buf), "FULL", 4) == 0)
    {
		return ALARMFULL;
	}
    if ( strncmp( trimwhitespace(buf), "OFF", 3) == 0)
    {
		return ALARMOFF;
	}
}

void checkAjaxStatus()
{
    int i;
    timeout.tv_sec  = 1;
    timeout.tv_usec = 0;
    FD_ZERO (&fsajax);
    FD_SET(fdajax, &fsajax);	
    
// 	read modem data
    int res = select ( fdajax+1 , &fsajax, NULL, NULL, &timeout );

    if (res == 0) {
		//printf("No Ajax Data\n");
        return;
    } else if (res > 0) {
        if ( FD_ISSET(fdajax, &fsajax) ) {
            memset(bufajax,0x00,sizeof(bufajax));
            res = read(fdajax,bufajax,512);

            for ( i= 0; i< res; i++)
            {
				ajaxAnswer[idxAjax++] = bufajax[i];
				if ( bufajax[i] == '\n')
				{
					ajaxAnswer[idxAjax++] = 0;
					idxAjax = 0;
				  
					if ( strstr( ajaxAnswer, "ALARM") != NULL )
					{
						printf("%s\n", ajaxAnswer);
						ajaxstatus = ST_AJAX_ALARM;
						if ( strstr( ajaxAnswer, ajaxMotionId) != NULL)
							ajaxAlarm = AJAX_ALARM_MOTION;
	
						if ( strstr( ajaxAnswer, ajaxSmokeId) != NULL)
							ajaxAlarm = AJAX_ALARM_SMOKE;
	
						if ( strstr( ajaxAnswer, ajaxDoorId) != NULL)
							ajaxAlarm = AJAX_ALARM_DOOR;
	
						if ( strstr( ajaxAnswer, ajaxKeyId) != NULL)
							ajaxAlarm = AJAX_ALARM_KEY;
							
						//if ( status == ST_MODEM_NETWORK_OK)
						//{
						//	callAlarm("");
						//	ajaxstatus = ST_AJAX_OK;
						//}	

					} 
					else if ( strstr( ajaxAnswer, "STATUS") != NULL )
					{
						// STATUS;2;001D65;3;46;58;37437;-103;-45;0;1;1;1;0;427;0;-127;0;
						// STATUS;1;001A04;2;53;65;23786;-102;-72;0;1;1;2;0;9882;0;-127;6;
						// STATUS;4;0036F4;1;80;92;23952;-103;-68;0;1;1;3;0;-732;0;-127;4;
						setAjaxStatus( ajaxAnswer);
						//printf("STATUS!\n");
					} 
					else if ( strstr( ajaxAnswer, "EVENT") != NULL )
					{
						printf("EVENT!\n");
					} 
				}
			}
        }
	}
}	

void setAjaxStatus( char *txt)
{
	char *id = getAjaxId(txt);
	
	printf("id=%s\n", id);
	int i;
	int newsensor = 1;
	for ( i= 0; i < SENSOR_COUNT; i++)
	{
		if ( strncmp( id, sensor[i].id, 6) == 0)
		{
			sensor[i].status = 1;
			newsensor= 0;
			break;
		}
	}
	if ( newsensor == 1)
		if ( sensor_count < SENSOR_COUNT)
		{
			strncpy( sensor[sensor_count].id , id, 6);
			sensor[sensor_count++].status = 1;
		}
			
	free (id);
			
}

char *getAjaxId( char *txt)
{
	//STATUS;4;0036F4;1;80;92;23952;-103;-68;0;1;1;3;0;-732;0;-127;4;
	if ( strlen( txt ) < 17) 
	  return "";
	
	char subStr[8];
	memset (subStr, 0, 8);
	strncpy(subStr, txt+9, 6);
	return strdup(subStr);
	
	//printf("getAjaxId = %s\n ", subStr);
}

void parseModemStatus()
{

	int i;

	timeout.tv_sec  = 1;
    timeout.tv_usec = 0;
    FD_ZERO (&fs);
    FD_SET(fd, &fs);	
    
// 	read modem data
    int res = select ( fd+1 , &fs, NULL, NULL, &timeout );

    if (res == 0) {
		//printf("No Data\n");
        return;
    } else if (res > 0) {
        if ( FD_ISSET(fd, &fs) ) {
            memset(buf,0x00,sizeof(buf));
            res = read(fd,buf,512);
			//printf("Read %d bytes\n", res);

            for ( i= 0; i< res; i++)
            {
				modemAnswer[idxAnswer++] = buf[i];
				if ( buf[i] == '\n')
				{
					modemAnswer[idxAnswer++] = 0;
					printf("%s", modemAnswer);
					idxAnswer = 0;
				  
					if ( strstr( modemAnswer, "+CPIN: READY") != NULL )
					{
						//printf("PIN OK\n");
						status = ST_MODEM_PIN_OK;
						modem.simstatus = 1;
					} 
					else if ( strstr ( modemAnswer, "+CPIN: NOT INSERTED") != NULL)
					{
						status = ST_MODEM_NO_SIM;
						modem.simstatus = -1;
					}
					else if ( strstr ( modemAnswer, "+CPIN: SIM PIN") != NULL)
					{
						status = ST_MODEM_PIN_SIM;
						inputPin("");
					}
					// +CREG: 0,5
					else if ( strstr( modemAnswer, "+CREG:") != NULL )
					{
						parseNetstatus ( modemAnswer);
						status = ST_MODEM_NETWORK_OK;
					} 
					else if ( strstr( modemAnswer, "NO DIALTONE") != NULL )
					{
						//printf("no dialtone \n");
						status = ST_MODEM_NO_DIALTONE;
					} 
					else if ( strstr( modemAnswer, "+CLCC: 1,0,0,0,0") != NULL )
					{
						printf("дозвонились! \n");
						status = ST_MODEM_CALL_OK;
					} 
					else if ( strstr( modemAnswer, "+CLCC: 1,0,3,0,0") != NULL )
					{
						printf("звонок \n");
					} 
					else if ( strstr( modemAnswer, "+CLCC: 1,0,6,0,0") != NULL )
					{
						status = ST_MODEM_NO_ANSWER;
					} 
					
					else if ( strstr( modemAnswer, "BUSY ") != NULL )
					{
						printf("занято! \n");
						status = ST_MODEM_BUSY;
					} 
					else if ( strstr( modemAnswer, "+CREC: 0") != NULL )
					{
						printf("музыка! \n");
						status = ST_MODEM_VOICE_OK;
					}
					//+CPAS: 0
					else if ( strstr( modemAnswer, "+CPAS: ") != NULL )
					{
						parseCpas( modemAnswer);
					} 
				 
					//+COPS: 0,0,"E-Plus"
					else if ( strstr( modemAnswer, "+COPS: ") != NULL )
					{
						parseCops( modemAnswer);
					} 
					//+CSPN: "mobilcom-debitel",0
					else if ( strstr( modemAnswer, "+CSPN: ") != NULL )
					{
						parseSIM( modemAnswer);
					} 
					else if ( strstr( modemAnswer, "+CSQ: ") != NULL )
					{
						parseLevel( modemAnswer);
					} 
					else if ( strstr( modemAnswer, "+DTMF:") != NULL )
					{
						if ( strstr( modemAnswer, "+DTMF: 9") != NULL )
						{
							status = ST_MODEM_DTFM_OK;
							printf("кнопка ОК! \n");
							//ajaxstatus = ST_AJAX_OK;
						}
						else if ( strstr( modemAnswer, "+DTMF: 0") != NULL )
						{
							printf("Меню! \n");
							status = ST_MODEM_CALL_OK;
							//ajaxstatus = ST_AJAX_ALARM;
						}
						else
						{
							status = ST_MODEM_CALL_OK;
							printf("кнопка ERROR! \n");
							//ajaxstatus = ST_AJAX_ALARM;
						}
					} 
					// +CREC: 0 - музыка прошла
					
				}
			}
        }
    }
    
    //printf("status=%d\n", status);
    if ( status == ST_MODEM_INIT)
    {
		
	} else if ( status == ST_MODEM_PIN_OK) {
		checkNetwork();
		status = ST_MODEM_NETWORK;
	} else if ( status == ST_MODEM_NETWORK_OK) {
		
	//} else if ( status == ST_MODEM_PIN_SIM ) {
	//	status = ST_MODEM_PIN;
    }
}
int playOKVoice()
{
	sendData(MUSIC_OK);
}
int playErrorVoice()
{
	sendData(MUSIC_ERROR);
}

int playVoice()
{
	if ( ajaxAlarm == AJAX_ALARM_MOTION)
		sendData(MUSIC_MOTION);
	if ( ajaxAlarm == AJAX_ALARM_SMOKE)
		sendData(MUSIC_SMOKE);
	if ( ajaxAlarm == AJAX_ALARM_DOOR)
		sendData(MUSIC_DOOR);
	if ( ajaxAlarm == AJAX_ALARM_KEY)
		sendData(MUSIC_KEY);
	return 0;
}	

int checkPin() 
{
	sendData("AT+CPIN?\n");
	printf("PIN eingabe \n");
	return 0;
}

int inputPin( char *pin)
{
	if ( strcmp( pin, "----") == 0)
		return 0; 
		
	//sendData("AT+CPIN=7723\n");
	sendData( pin);
	return 0;
}	
void writeModemStatus()
{
	FILE *fp;
	char buf[BUF_SIZE];
	
	memset( buf, 0, BUF_SIZE);
	
	if ((fp = fopen(FILE_ALARMNET, "w"))==NULL) {
		return;
	}

	sprintf( buf, "%s\n%d\n%d\n%d\n", modem.netname, modem.level, modem.netstatus,  modem.simstatus);
	
	fwrite( buf, sizeof(char), strlen(buf), fp);
	fclose(fp);	
	
	return;
	
}
void writeAjaxStatus()
{
	
	FILE *fp;
	char buf[BUF_SIZE];
	//struct tm *u;
	int i;
	//const time_t timer = time(NULL);
  
	//u = localtime(&timer);
	
	memset( buf, 0, BUF_SIZE);
	
	if ((fp = fopen(FILE_AJAX, "w"))==NULL) {
		return;
	}
	//strftime(buf, 40, "%d.%m.%Y %H:%M:%S\n", u);
	
	
	for ( i= 0; i < sensor_count; i++)
	{
		strcat( buf, sensor[i].id);
		strcat( buf, "\n");
	}
	
	fwrite( buf, sizeof(char), strlen(buf), fp);
	fclose(fp);	
	
	return;
	
}

int openModem()
{
    fd = open(ttyModem, O_RDWR | O_NONBLOCK | O_NOCTTY | O_NDELAY);
    if (fd == -1) {
        perror("Unable to open modem port");
        return ERROR;
    }

    // clear all file status flags
    fcntl(fd, F_SETFL, 0);

    memset(&term, 0, sizeof(struct termios));

    if ((cfsetispeed(&term, B115200) < 0) ||
        (cfsetospeed(&term, B115200) < 0)) {
            perror("Unable to set baudrate");
            return ERROR;
    }

    term.c_cflag |= (CREAD | CLOCAL);
    term.c_cflag &= ~CSIZE;
    term.c_cflag |= CS8;

    term.c_cflag &= ~CSTOPB;

    term.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    term.c_iflag &= ~(INPCK);
    term.c_oflag &= ~OPOST;

    if (tcsetattr(fd, TCSANOW, &term) < 0)
    {
        perror("Unable to set port parameters");     
        return ERROR;
    }
}

int initAjax()
{
    fdajax = open(ttyAjax, O_RDWR | O_NONBLOCK | O_NOCTTY | O_NDELAY);
    if (fdajax == -1) {
        perror("Unable to open Ajax port");
        return ERROR;
    }

    // clear all file status flags
    fcntl(fdajax, F_SETFL, 0);

    memset(&termajax, 0, sizeof(struct termios));

    if ((cfsetispeed(&termajax, B9600) < 0) ||
        (cfsetospeed(&termajax, B9600) < 0)) {
            perror("Unable to set baudrate");
            return ERROR;
    }

    termajax.c_cflag |= (CREAD | CLOCAL);
    termajax.c_cflag &= ~CSIZE;
    termajax.c_cflag |= CS8;

    termajax.c_cflag &= ~CSTOPB;

    termajax.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    termajax.c_iflag &= ~(INPCK);
    termajax.c_oflag &= ~OPOST;

    if (tcsetattr(fdajax, TCSANOW, &termajax) < 0)
    {
        perror("Unable to set port parameters");     
        return ERROR;
    }
    return OK;
	
}
int Sim800_WriteCmdWithCheck (const char *cmd)
{
    sendData(cmd); /// Запись команды

    return Sim800_CompareStr("\r\nOK"); /// Функция чтения данных с порта UART с парсингом ответа
}

int Sim800_CompareStr (char *str)
{
    if( strstr(buf, str) == NULL )
        return 0;

    return 1;
}

void nextPhone()
{
	if ( ++actphone >= PHONE_COUNT)
	{
		actphone = 0;
	}
	printf("Phone index = %d\n", actphone);
}

int readAlarm()
{
	int fda;
	ssize_t ret;
	char ch[256];

	fda = open ("/tmp/alarm", O_RDONLY);
	if (fda < 0)
	{
		return 1;
	}	
	
	ret = read (fda, &ch, 256);
	if (ret < 0)
	{
        return 2;
	}
	close (fda);    
    if ( ret > 5)
        if ( strncmp(ch, "ALARM", 5)== 0)
            return -1;
    return 0;
}
char *trimwhitespace(char *str)
{
  char *end;

  // Trim leading space
  while(isspace((unsigned char)*str)) str++;

  if(*str == 0)  // All spaces?
    return str;

  // Trim trailing space
  end = str + strlen(str) - 1;
  while(end > str && isspace((unsigned char)*end)) end--;

  // Write new null terminator
  *(end+1) = 0;

  return str;
}


int loadphones( int check)
{
	FILE *fp, *fp2;
	char buf[256];
	
	if ( check == 1)
	{
		if ((fp = fopen( FILE_CELL_NEW, "r"))==NULL) {
			return 0;
		}
		fclose(fp);	
	}	
	memset( buf, 0, 256);

	if ((fp2 = fopen( FILE_CELL, "r"))==NULL) {
		return -1;
	}

	int rbytes= fread( buf, sizeof(char), 256, fp2);
	fclose(fp2);	
	
	char* token;
	char* string;
	char* tofree;
	
	string = strdup( buf);

	if (string != NULL) 
	{
		tofree = string;
		int i= 0;
		while ((token = strsep(&string, "\n")) != NULL)
		{
			if ( i < PHONE_COUNT) 
			{
				strncpy( phone[i], "ATDP", 4);
				strcat( phone[i], token);
				strcat( phone[i], ";\r");
				i++;
			}
		}
		free(tofree);
	}
	remove( FILE_CELL_NEW);
	
	return 0;
	
}

int main()
{
  
    
	int i= 0;
	idxAnswer = 0;
	int cycle = 9;
	
	parse_ini_file( "/etc/sim800.ini");
	
//	strcpy( phone[0], "ATDP+4917661095978;\r");
//	strcpy( phone[1], "ATDP+4917661095978;\r");
//	strcpy( phone[2], "ATDP+4917661095978;\r");
	
	loadphones( 0);	
	actphone = 0;
	printf("Phone=%s\n", phone[0]);
	printf("Phone=%s\n", phone[1]);
	printf("Phone=%s\n", phone[2]);
	printf("OK 3 \n");
	
	strcpy( music[0], "AT+CREC=4,C:\\User\\motion.amr,0,100\n");
	strcpy( music[1], "AT+CREC=4,C:\\User\\smoke.amr,0,100\n");
	strcpy( music[2], "AT+CREC=4,C:\\User\\door.amr,0,100\n");
	strcpy( music[3], "AT+CREC=4,C:\\User\\repeat.amr,0,100\n");
	strcpy( music[4], "AT+CREC=4,C:\\User\\error.amr,0,100\n");
	strcpy( music[5], "AT+CREC=4,C:\\User\\ok.amr,0,100\n");

	//printf("OK 4 \n");
	
    if ( openModem() == ERROR)
    {
		status = ST_MODEM_ERROR;
		printf("Modem open error\n");
	}
	else
		printf("Modem open OK\n");
	
    if ( initAjax() == ERROR)
    {
		ajaxstatus = ST_AJAX_ERROR;
		printf("Ajax open error\n");
	}
	else
		printf("Ajax open OK\n");
    
    ajaxstatus = ST_AJAX_OK;
    
    initModem();
    status = ST_MODEM_INIT;
	printf("initModem OK\n");
    sleep(3);
     
    
	checkPin();
	status = ST_MODEM_PIN;
    
    while(1)
    {
		
		loadphones( 1);	

		//tm= 3;
		parseModemStatus();
		if ( ++cycle == 10 && status!=ST_AJAX_ALARM)
		{
			cycle = 0;
			checkModemStatus();
		}
		checkAjaxStatus();

		writeModemStatus();
		writeAjaxStatus();
		
		//for ( int i=0; i < SENSOR_COUNT; i++)
		//  printf( "i= %d id=%s\n", i, sensor[i].id);
		
		printf( "SIM=%s NET=%s LEVEL=%d STATUS=%d AJAX=%d\n", modem.simname, modem.netname, modem.level, status, ajaxstatus);
		
		if ( checkAlarmStatus() != ALARMFULL)
		{
			// alarm off
			continue;
		}
		
		if ( ajaxstatus == ST_AJAX_ALARM )
		{   
			ajaxstatus = ST_AJAX_ALARM_WAIT;
			if ( status == ST_MODEM_NETWORK_OK)
			{
				callAlarm("");
			}
		}
		if ( ajaxstatus == ST_AJAX_ALARM_WAIT )
		{
			if ( status == ST_MODEM_CALL_OK)
			{
				status = ST_MODEM_VOICE;
				playVoice( MUSIC_MOTION);
			}
			if ( status == ST_MODEM_DTFM_OK)
			{
				// playVoice ?
				playVoice( MUSIC_OK);
				hangup();
				ajaxstatus = ST_AJAX_OK;
				actphone = 0;
			}
			if ( status ==ST_MODEM_NO_ANSWER) // номер не прошел
			{
				nextPhone();
				status = ST_MODEM_NETWORK_OK;
				ajaxstatus = ST_AJAX_ALARM;
			}
			if ( status == ST_MODEM_VOICE_OK)
			{
				if ( ++voice_counter > VOICE_TIMEOUT) // кнопочку не нажали!
				{
					voice_counter = 0;
					nextPhone();
					// отбиваем
					hangup();
					status = ST_MODEM_NETWORK_OK;
					ajaxstatus = ST_AJAX_ALARM;
				}
			}
			

		}
		if ( ajaxstatus == ST_AJAX_OK)
		{
			if ( status ==ST_MODEM_NO_ANSWER)
				status = ST_MODEM_NETWORK_OK;
		}
	}

}

int initModem()
{

    if (Sim800_WriteCmdWithCheck("ATE0\n"))
        printf("check 1 OK\n");
    sleep(1);
    if (Sim800_WriteCmdWithCheck("AT+CMEE=1\n"))
        printf("check 2 OK\n");
    sleep(1);
    if (Sim800_WriteCmdWithCheck("ATV1\n"))
        printf("check 3 OK\n");
    sleep(1);
    if (Sim800_WriteCmdWithCheck("AT+GSMBUSY=1\n"))
        printf("check 4 OK\n");
   sleep(1);
    if (Sim800_WriteCmdWithCheck("AT+DDET=1,0,1\n"))
        printf("check 5 OK\n");
    sleep(1);    
    if (Sim800_WriteCmdWithCheck("AT+CSPN?\n"))
        printf("check 6 OK\n");
        
       sleep(1);    
    
}
void checkCall()
{
    Sim800_WriteCmdWithCheck("AT+CLCC\n");
}
void checkCpas()
{
    Sim800_WriteCmdWithCheck("AT+CPAS\n");
}
void checkModemStatus()
{

	checkCpas();

	if ( modem.status == CPAS_VOICE)
	  return;

    Sim800_WriteCmdWithCheck("AT+CREG?\n");
    //sleep(1);
    //if (
    Sim800_WriteCmdWithCheck("AT+COPS?\n");
    //sleep(1);
    //if (
    Sim800_WriteCmdWithCheck("AT+CSQ\n");
    //sleep(1);
}


void checkNetwork()
{
	sendData("AT+CREG?\r");
	
    //if ( Sim800_WriteCmdWithCheck("AT+CREG?\n"))
    //    printf("check 5 OK\n");
    //sleep(1);
    //if ( Sim800_WriteCmdWithCheck("AT+COPS?\n"))
    //    printf("check 6 OK\n");
    //status = ST_MODEM_NETWORK;    
}

int sendData( char *str)
{
	printf("%s\n", str);
        return write ( fd, str, strlen(str));
}
	

int getData()
{
    int res = select ( fd+1 , &fs, NULL, NULL, &timeout );

    if (res == 0) {
        //perror("Timeout occurs");
    } else if (res > 0) {
        if ( FD_ISSET(fd, &fs) ) {
            memset(buf,0x00,sizeof(buf));
            res = read(fd,buf,512);
            printf("%s\n", buf);
            //if ( strstr( buf, "+COPS: 0,0,"))
        }
    }

}

int parse_ini_file(char * ini_name)
{
    dictionary  *   ini ;
    const char * s;


    ini = iniparser_load(ini_name);
    if (ini==NULL) {
        fprintf(stderr, "cannot parse file: %s\n", ini_name);
        return -1 ;
    }


	strncpy( ajaxSmokeId, iniparser_getstring(ini, "Sensors:smoke", NULL), MAXID);
	strncpy( ajaxMotionId, iniparser_getstring(ini, "Sensors:motion", NULL), MAXID);
	strncpy( ajaxKeyId, iniparser_getstring(ini, "Sensors:key", NULL), MAXID);
	strncpy( ajaxDoorId, iniparser_getstring(ini, "Sensors:door", NULL), MAXID);


	printf( "Motion = %s\n", ajaxMotionId);	
	printf( "Smoke = %s\n", ajaxSmokeId);	
	printf( "Door = %s\n", ajaxDoorId);	
	printf( "Key = %s\n", ajaxKeyId);	
   
   

	strncpy ( ttyAjax, iniparser_getstring(ini, "Main:Ajax", "/dev/ttyACM0"), 64);
	strncpy ( ttyModem, iniparser_getstring(ini, "Main:Modem", "/dev/ttyUSB0"), 64);
	sprintf ( pin, "AT+CPIN=%s\n", iniparser_getstring(ini, "Main:PIN", "----"));

    printf("ajax %s\n", ttyAjax);
    printf("modem %s\n", ttyModem);
    printf("pin %s\n", pin);

    iniparser_freedict(ini);
    return 0 ;
}


