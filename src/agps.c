#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/select.h>
#include <termios.h> 
#include <string.h>
#include <assert.h>

#include "iniparser.h"

#define FILE_GPS		"/var/ig_gps.dat"
#define BUF_SIZE 512 //размер буфера обязательно равен степени двойки!
#define BUF_MASK (BUF_SIZE-1)

int	parse_ini_file(char * ini_name);
void parseGPSStatus();
void writeGPS();

int	fd;
fd_set fs;
struct timeval timeout;
struct termios term;



char ttyGPS[64];
char buf[BUF_SIZE];

char gpsAnswer[BUF_SIZE];
int idxAnswer;


int main()
{

	parse_ini_file( "/etc/sim800.ini");
	printf("%s\n", ttyGPS);	
	
	fd = open(ttyGPS, O_RDONLY | O_NONBLOCK | O_NOCTTY | O_NDELAY);		

	int i;

	while (1)
	{
		parseGPSStatus();
	}
    

	
}

int parse_ini_file(char * ini_name)
{
    dictionary  *   ini ;
    const char * s;


    ini = iniparser_load(ini_name);
    if (ini==NULL) {
        fprintf(stderr, "cannot parse file: %s\n", ini_name);
        return -1 ;
    }

	strncpy ( ttyGPS, iniparser_getstring(ini, "Main:GPS", "/dev/ttyUSB4"), 64);

    iniparser_freedict(ini);
    return 0 ;
}

void parseGPSStatus()
{

	int i;
	int res;

	timeout.tv_sec  = 1;
    timeout.tv_usec = 0;
    FD_ZERO (&fs);
    FD_SET(fd, &fs);	
    
// 	read modem data
    res = select ( fd+1 , &fs, NULL, NULL, &timeout );

    if (res == 0) {
		//printf("No Data\n");
        return;
    } else if (res > 0) {
        if ( FD_ISSET(fd, &fs) ) {
            memset(buf,0x00,sizeof(buf));
            res = read(fd,buf,512);

            for ( i= 0; i< res; i++)
            {
				gpsAnswer[idxAnswer++] = buf[i];
				if ( buf[i] == '\n')
				{
					gpsAnswer[idxAnswer++] = 0;
					idxAnswer = 0;
				  
					if ( strstr( gpsAnswer, "$GPRMC") != NULL )
					{
						writeGPS();
					} 
				}
			}
        }
    }
    
}

void writeGPS()
{

	FILE *fp;
	char buf[BUF_SIZE];
	
	memset( buf, 0, BUF_SIZE);
	
	if ((fp = fopen(FILE_GPS, "w"))==NULL) {
		return;
	}
	
	fwrite( gpsAnswer, sizeof(char), strlen(gpsAnswer), fp);
	fclose(fp);	
	
	return;

}

