
// +CLCC: 1,0,2,0,0,"4917661095978",129,""
// +CLCC: 1,0,3,0,0,"4917661095978",129,""
// +CLCC: 1,0,0,0,0,"4917661095978",129,""

#define SHARED_MEMORY_OBJECT_NAME "my_shared_memory"
#define SHARED_MEMORY_OBJECT_SIZE 50
#define SHM_CREATE 1
#define SHM_PRINT  3
#define SHM_CLOSE  4

#define BUF_SIZE 512 //размер буфера обязательно равен степени двойки!
#define BUF_MASK (BUF_SIZE-1)


#define ST_MODEM_START			0
#define ST_MODEM_INIT			1
#define ST_MODEM_INIT_OK		2
#define ST_MODEM_INIT_ERROR		3
#define ST_MODEM_PIN			4
#define ST_MODEM_PIN_OK			5
#define ST_MODEM_PIN_ERROR		6
#define ST_MODEM_PIN_SIM		23
#define ST_MODEM_NO_SIM			24
#define ST_MODEM_NETWORK		7
#define ST_MODEM_NETWORK_OK		8
#define ST_MODEM_NETWORK_ERROR	9
#define ST_MODEM_CALL			10
#define ST_MODEM_NO_DIALTONE    11
#define ST_MODEM_BUSY 			12
#define ST_MODEM_NO_CARRIER     13
#define ST_MODEM_ERROR			14

#define ST_MODEM_VOICE			15
#define ST_MODEM_VOICE_OK		16
#define ST_MODEM_VOICE_ERROR	17
#define ST_MODEM_CALL_OK		18
#define ST_MODEM_NO_ANSWER		19

#define ST_MODEM_DTFM_OK		20
#define ST_MODEM_DTFM_RPT		21
#define ST_MODEM_DTFM_ERROR		22


#define ST_AJAX_START	0
#define ST_AJAX_ERROR	1
#define ST_AJAX_OK		2
#define ST_AJAX_ALARM	3
#define ST_AJAX_ALARM_WAIT	4


#define SENSOR_COUNT	3
#define VOICE_TIMEOUT	7 		
#define PHONE_COUNT 	3

#define FILE_ALARMNET 	"/tmp/ig_alarmnet.dat"
#define FILE_VIDEONET 	"/tmp/ig_videonet.dat"
#define FILE_AJAX	  	"/tmp/ig_ajax.dat"
#define FILE_CELL	  	"/var/ig_cell.dat"
#define FILE_CELL_NEW	"/tmp/ig_cell.new"
#define FILE_GPS		"/var/ig_gps.dat"
#define FILE_ALARM 		"/var/ig_alarm.dat"

#define ALARMFULL 	1
#define ALARMOFF 	0
#define ALARMERROR 	-1



/*
#define ST_MODEM_READY			2
#define ST_MODEM_NO_DIALTONE    4
#define ST_MODEM_BUSY 			5
#define ST_MODEM_NO_CARRIER     6
#define ST_MODEM_NO_ANSWER      7
*/

#define ERROR	-1
#define OK		 0

#define AJAX_NO			0
#define AJAX_ALARM		1 

#define MAXNAME 	63
#define MAXID	10

#define NET_NO		0 // не зарегистрирован, поиска сети нет
#define NET_HOME	1 // зарегистрирован, домашняя сеть
#define NET_FIND	2 // не зарегистрирован, идёт поиск новой сети
#define NET_DENIED	3 // регистрация отклонена
#define NET_UNKNOWN	4 // неизвестно
#define NET_ROAMING	5 // роуминг


#define CPAS_READY		0 // готов к работе
#define CPAS_UNKNOWN	2 // неизвестно
#define CPAS_INCOMING	3 // входящий звонок
#define CPAS_VOICE		4 // голосовое соединение

#define	AJAX_ALARM_NO		0
#define AJAX_ALARM_MOTION	1
#define AJAX_ALARM_SMOKE	2
#define AJAX_ALARM_DOOR		3
#define AJAX_ALARM_KEY		4


#define MUSIC_MOTION	"AT+CREC=4,C:\\User\\motion.amr,0,100\n"	// движение  
#define MUSIC_SMOKE		"AT+CREC=4,C:\\User\\smoke.amr,0,100\n"		// дым
#define MUSIC_DOOR		"AT+CREC=4,C:\\User\\door.amr,0,100\n"		// двери
#define MUSIC_REPEAT	"AT+CREC=4,C:\\User\\repeat.amr,0,100\n"	// для повтора нажмите
#define MUSIC_KEY		"AT+CREC=4,C:\\User\\key.amr,0,100\n"		// брелок
#define MUSIC_ERROR		"AT+CREC=4,C:\\User\\error.amr,0,100\n"		// код не распознан
#define MUSIC_OK		"AT+CREC=4,C:\\User\\ok.amr,0,100\n"		// до свидания


struct {
	int reg;
	int netstatus;
	char simname[MAXNAME];
	char netname[MAXNAME];
	int level;
	int status; //CPAS
	int simstatus;
} modem;

struct {
	char id[8];
	int status;
	int signal;
	int battery;
} sensor[SENSOR_COUNT];
int sensor_count = 0;

//int getIndById( char *);
char *getAjaxId( char *txt);
void setAjaxStatus( char *txt);

int initModem();
int initAjax();

int readAjax();
char** str_split(char* a_str, const char a_delim);

void hangup();

char *getName( char *txt);
void parseCops(char *txt);
void parseSIM(char *txt);
void parseLevel( char *txt);
void parseCpas( char *txt);
void parseNetstatus (char *txt);
int  parse_ini_file(char * ini_name);

void nextPhone();

int openModem();
void checkCall();
void checkCpas();
void checkModemStatus();
void parseModemStatus();
void checkAjaxStatus();
void writeModemStatus();
void writeAjaxStatus();

void checkNetwork();
int checkReady();
int checkPin();
int inputPin( char *);
int checkReg();
int callAlarm( char *);
int playVoice();
int playOKVoice();
int playErrorVoice();

char *trimwhitespace(char *str);

