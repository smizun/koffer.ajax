#=====================================
# OpenWrt Makefile
#=====================================

include $(TOPDIR)/rules.mk

PKG_NAME:=ajax
PKG_VERSION:=1.1.1
PKG_RELEASE:=1

PKG_BUILD_DIR:= $(BUILD_DIR)/$(PKG_NAME)
#PKG_BUILD_DEPENDS:=libpcap

include $(INCLUDE_DIR)/kernel.mk
include $(INCLUDE_DIR)/package.mk


define Package/ajax
	SECTION:=utils
	CATEGORY:=Utilities
	TITLE:=Ajax Bridge Utility
	MAINTAINER:=Pavlo Bezhan <pbezhan@gmail.com>
endef

TARGET_CPPFLAGS := \
	-D_GNU_SOURCE \
	-I$(PKG_BUILD_DIR) \
	$(TARGET_CPPFLAGS) \
	-I$(LINUX_DIR)/user_headers/include


define Package/ajax/description
	Ajax Bridge utility
endef

define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) ./src/* $(PKG_BUILD_DIR)/
endef


define Build/Compile
	CFLAGS="$(TARGET_CPPFLAGS) $(TARGET_CFLAGS)" \
	$(MAKE) -C $(PKG_BUILD_DIR) \
		$(TARGET_CONFIGURE_OPTS) \
		LIBS="$(TARGET_LDFLAGS)"
endef



define Package/ajax/install
	$(INSTALL_DIR) $(1)/bin
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/ajax $(1)/
endef

$(eval $(call BuildPackage,ajax))

